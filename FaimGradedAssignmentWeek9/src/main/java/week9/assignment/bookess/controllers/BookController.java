package week9.assignment.bookess.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import week9.assignment.bookess.entities.Book;
import week9.assignment.bookess.service.BookService;

@Controller
public class BookController {

	@Autowired
	private BookService bookService;
	
	@GetMapping("/add")
	public String addBook() {
		System.out.println("Admin Page method addBook() called");
		return "add";
	}
	
	@PostMapping("/addbook")
	public String addBook(Book book,HttpSession session) {
		System.out.println("Book Controller addBook() is called");
		boolean isAdded = this.bookService.addBookDetails(book);
		
		if(isAdded) {
			session.setAttribute("mssg", "Book with Name " + book.getBookName() + " Successfully Added");
			return "redirect:/message";
		}else {
			System.out.println("Boook is Already present in Database");
			return "redirect:/message";
		}
	}
	
	
	@GetMapping("/delete")
	public String deleteBook() {
		System.out.println("deleteBook() from Book Controller is called");
		return "delete";
	}

	@GetMapping("/book")
	public String getAllBooks(Map<String,List<Book>> map,HttpSession session) {
		List<Book> listOfAllBooks = bookService.getAllBooks();
		map.put("listOfAllBooks", listOfAllBooks);
		if(listOfAllBooks.isEmpty()) {
			session.setAttribute("mssg", "List is Empty");
			return "redirect:/message";
		}else {
			for(Book book : listOfAllBooks) {
				System.out.println(book);
			}
			return "book";
		}
	}
	
	@PostMapping("/deletebook")
	public String deleteBookById(@RequestParam("bookId") int id,HttpSession session) {
		System.out.println("deleteBookById() from BookController is called");
		boolean isDeleted = this.bookService.deleteBook(id);
		if(isDeleted) {
			session.setAttribute("mssg", "Book With Id " + id + " has been Deleted");
			return "redirect:/message";
		}else {
			session.setAttribute("mssg", "Book With id " + id + " Does not Exists");
			return "redirect:/message";
		}
	}
	
	@GetMapping("/edit/{bookId}")
	public String getBookForUpdation(@PathVariable int bookId,Map<String,Integer> map) {
		System.out.println("getBookForUpdation() is from BookController called");
		map.put("bookId", bookId);
		return "edit";
	}
	
	@PostMapping("/update")
	public String updateBookById(Book book,HttpSession session) {
		System.out.println("updateBookById() from BookController is called");
		System.out.println(book);
		boolean isupdated = this.bookService.updateBook(book);
		if(isupdated) {
			session.setAttribute("mssg", "Book With Id " + book.getBookId() + " has been Updated");
			return "redirect:/message";
		}else {
			session.setAttribute("mssg", "Book With id " + book.getBookId() + " Does not Exists");
			return "redirect:/message";
		}
	}
}
