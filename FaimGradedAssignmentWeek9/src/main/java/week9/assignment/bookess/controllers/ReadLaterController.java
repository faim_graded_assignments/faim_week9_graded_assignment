package week9.assignment.bookess.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import week9.assignment.bookess.entities.Book;
import week9.assignment.bookess.entities.ReadLater;
import week9.assignment.bookess.service.BookService;
import week9.assignment.bookess.service.ReadLaterBookService;

@Controller
public class ReadLaterController {
	
	@Autowired
	private ReadLaterBookService readLaterBookService;
	
	@Autowired
	private BookService bookService;
	
	@GetMapping("/read")
	public String getReadLaterBooks(Map<String, List<ReadLater>> map,HttpSession session) {
		System.out.println("getReadLaterBooks() from ReadLaterController is called");
		
		List<ReadLater> listOfReadLater = this.readLaterBookService.getAllReadLaterBooks();
		
		map.put("listOfReadLater", listOfReadLater);
		if(listOfReadLater.isEmpty()) {
			session.setAttribute("mssg", "Book List is empty");
			return "redirect:/message";
		}
		return "readlater";
	}
	
	@RequestMapping(value = "/addread/{id}",method = RequestMethod.GET)
	public String getBookById(@PathVariable int id,HttpSession session) {
		
		System.out.println("getBookById() from ReadLaterController is called");
		
		Book book = this.bookService.getBook(id);
		if(book == null) {
			System.out.println("getBook returns null book "+book);
			session.setAttribute("mssg", "Book Id Does not Exists");
			return "redirect:/message";
		}else {
			ReadLater readLater = new ReadLater(id, book.getBookName(), book.getBookGenre(),(String)session.getAttribute("email"));
			boolean isAdded = this.readLaterBookService.addToReadLater(readLater);
			if(isAdded){
				System.out.println("Book Added to ReadLater");
				session.setAttribute("mssg", "Book Added to ReadLater");
				return "redirect:/message";
			}else {
				session.setAttribute("mssg", "Book is already Present is the ReadLater Section");
				return "redirect:/message";
			}
		}
	}
	
	@GetMapping(value = "/removeread/{id}")
	public String deleteById(@PathVariable int id,HttpSession session){
		System.out.println("deleteById() from ReadLaterController is called");
		boolean isDeleted = this.readLaterBookService.deleteBook(id);
		
		if(isDeleted) {
			System.out.println("Book is deleted from read later");
			session.setAttribute("mssg", "Book with id " + id + " Removed From Read Later");
			return "redirect:/message";
		}else {
			return "dashboard";
		}
	}
}
