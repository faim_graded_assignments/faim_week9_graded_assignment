package week9.assignment.bookess.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import week9.assignment.bookess.entities.ReadLater;

@Repository
public interface ReadLaterRepository extends CrudRepository<ReadLater, Integer>{

}
