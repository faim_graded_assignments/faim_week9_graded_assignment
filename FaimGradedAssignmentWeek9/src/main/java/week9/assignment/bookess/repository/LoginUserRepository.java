package week9.assignment.bookess.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import week9.assignment.bookess.entities.LoginUser;

@Repository
public interface LoginUserRepository extends CrudRepository<LoginUser, String>{

}
