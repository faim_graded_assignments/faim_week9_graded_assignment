package week9.assignment.bookess.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import week9.assignment.bookess.entities.Book;

@Repository
public interface BookRepository extends CrudRepository<Book,Integer>{
	
}
