package week9.assignment.bookess.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Book {
	
	@Id
	@Column(columnDefinition = "int(10)")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bookId;
	
	@Column(columnDefinition = "varchar(15)")
	private String bookName;
	
	@Column(columnDefinition = "varchar(15)")
	private String bookGenre;
	
	public Book() {
		System.out.println("Book Class Constructor");
	}

	public Book(String bookName, String bookGenre) {
		super();
		this.bookName = bookName;
		this.bookGenre = bookGenre;
	}
	
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public int getBookId() {
		return bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookGenre() {
		return bookGenre;
	}

	public void setBookGenre(String bookGenre) {
		this.bookGenre = bookGenre;
	}

	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", bookName=" + bookName + ", bookGenre=" + bookGenre + "]";
	}
	
}
